import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { WeatherProvider } from '../../providers/weather/weather';
// import { WeatherProvider } from '@myProviders/weather/weather'
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  weather: any;
  location: {
    city: string,
    state: string
  }

  constructor(
    public navCtrl: NavController,
    private weatherProvider: WeatherProvider,
    private storage: Storage) {

  }

  ionViewWillEnter() {
    this.storage.get('location').then((val) => {
      if (val != null) {
        this.location = JSON.parse(val);
      } else {
        this.location = {
          city: 'Berlin',
          state: 'DE'
        }
      }

      this.weatherProvider.getWeather(this.location.city, this.location.state)
        .subscribe(weather => {
          if (weather.current_observation === undefined) {
            // nothing found with the state -> it is a country 
            let results = weather.response.results;
            let found = results.find(x => x.country_iso3166 === this.location.state);

            this.weatherProvider.getWeatherWithAutoComplete(found.l)
              .subscribe(weather => {
                this.weather = weather.current_observation;
              });
          } else {
            this.weather = weather.current_observation;
          }
        });

    });
  }
}

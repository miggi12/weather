import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the WeatherProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class WeatherProvider {

  apiKey = 'f121a52db1ae1ca6';
  url;

  constructor(public http: HttpClient) {
    this.url = 'https://api.wunderground.com/api/' + this.apiKey + '/conditions';
  }

  getWeather(city, state): Observable<any> {
    return this.http.get(this.url + '/q' + '/' + state + '/' + city + '.json');
  }

  getWeatherWithAutoComplete(query): Observable<any> {
    return this.http.get(this.url + '/' + query + '.json');
  }

  getCity(city, country) {
    let link = 'http://autocomplete.wunderground.com/aq?query=' + city + '&c=' + country;
    return this.http.get(link);
  }

}
